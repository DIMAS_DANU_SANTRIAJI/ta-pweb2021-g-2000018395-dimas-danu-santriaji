<?php
$arrwarna = array("Red","Orange","Yellow","Green","Blue","Purple");

echo "Menampilkan isi array dengan FOR : <br>";
for($i=0; $i<count($arrwarna); $i++){
    echo "Warna pelangi <font color=$arrwarna[$i]>".$arrwarna[$i]."</font><br>";
}

echo "<br>Menampilkan isis array dengan Foreach: <br>";
foreach($arrwarna as $warna){
    echo "Warna pelangi <font color=$warna>".$warna."</font><br>";
}
?>